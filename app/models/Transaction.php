<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 13.01.14
 * Time: 15:50
 */

class Transaction extends Eloquent
{
    public function customer()
    {
        return $this->belongsTo('Customer');
    }
}