<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 13.01.14
 * Time: 15:46
 */

class Customer extends Eloquent
{
    public function transactions()
    {
        return $this->hasMany('Transaction');
    }
}