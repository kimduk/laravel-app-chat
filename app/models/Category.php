<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 05.02.14
 * Time: 10:48
 */

class Category extends Eloquent
{
    /**
     * Name of the DB table
     * @var string
     */
    protected $table = "category";

    protected $guarded = ["id"];

    protected $softDelete = true;

    public function products()
    {
        return $this->hasMany("Product");
    }
} 