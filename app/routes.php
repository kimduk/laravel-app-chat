<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

App::bind('Formativ\Billing\GatewayInterface', 'Formativ\Billing\StripeGateway');
App::bind('Formativ\Billing\DocumentInterface', 'Formativ\Billing\PDFDocument');
App::bind('Formativ\Billing\MessengerInterface', 'Formativ\Billing\EmailMessenger');

Route::get('/', function () {
    return View::make('index/index');
});

Route::any("/", [
    "as"   => "index/index",
    "uses" => "IndexController@indexAction"
]);

Route::controller('/customers', 'CustomerController');

Route::controller('/transactions', 'TransactionController');

Route::group(["before" => "guest"], function () {
    Route::any("/login", [
        "as" => "user/login",
        "uses" => "UserController@loginAction"
    ]);
    Route::any("/request", [
        "as" => "user/request",
        "uses" => "UserController@requestAction"
    ]);
    Route::any("/reset", [
        "as" => "user/reset",
        "uses" => "UserController@resetAction"
    ]);
});

Route::group(["before" => "auth"], function () {
    Route::any("/profile", [
        "as" => "user/profile",
        "uses" => "UserController@profileAction"
    ]);
    Route::any("/logout", [
        "as" => "user/logout",
        "uses" => "UserController@logoutAction"
    ]);
});

Route::any("category/index", [
    "as"   => "category/index",
    "uses" => "CategoryController@indexAction"
]);

Route::any("product/index", [
    "as"   => "product/index",
    "uses" => "ProductController@indexAction"
]);

Route::any("account/authenticate", [
    "as"   => "account/authenticate",
    "uses" => "AccountController@authenticateAction"
]);

Route::any("order/add", [
    "as"   => "order/add",
    "uses" => "OrderController@addAction"
]);

