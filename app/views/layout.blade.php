<!DOCTYPE html>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="/css/layout.css" />

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>

    <title>Lavarel4 Try</title>
</head>
<body>
@include("header")
<div class="content">
    <div class="container">
        @yield("content")
    </div>
</div>
@include("footer")
</body>
</html>