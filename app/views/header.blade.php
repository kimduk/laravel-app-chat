@section("header")
<div class="header">
    <div class="container">
        <h1>My Auth try</h1>
        @if (Auth::check())
            <a href="{{ URL::route('user/logout') }}">Logout</a>
            |
            <a href="{{ URL::route('user/profile') }}">Profile</a>
        @else
            <a href="{{ URL::route("user/login") }}">LogIn</a>
        @endif
    </div>
</div>
@show