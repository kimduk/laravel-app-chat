<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 05.02.14
 * Time: 11:58
 */

class OrderTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        $accounts = Account::all();

        foreach ($accounts as $account)
        {
            for ($i = 0; $i < rand(-1, 5); $i++)
            {
                Order::create([
                    "account_id" => $account->id
                ]);
            }
        }
    }
} 