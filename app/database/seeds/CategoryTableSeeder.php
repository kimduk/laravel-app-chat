<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 05.02.14
 * Time: 11:43
 */

class CategoryTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        $faker = $this->getFaker();

        for ($i = 0; $i < 10; $i++) {
            $name = ucwords($faker->word);

            Category::create([
                "name" => $name
            ]);
        }
    }
} 