<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 05.02.14
 * Time: 11:20
 */

class AccountTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        $faker = $this->getFaker();

        for ($i = 0; $i < 10; $i++) {
            $email = $faker->email;

            $password = Hash::make("password");

            Account::create([
                "email" => $email,
                "password" => $password
            ]);
        }
    }
} 