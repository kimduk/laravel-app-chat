<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 31.01.14
 * Time: 15:05
 */

class UserSeeder extends DatabaseSeeder
{

    public function run()
    {
        $users = [
            [
                "username" => "maslennikov.ilya",
                "password" => Hash::make("admin"),
                "email" => "maslo@example.ru"
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }

} 