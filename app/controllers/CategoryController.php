<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 05.02.14
 * Time: 12:25
 */

class CategoryController extends BaseController
{
    public function indexAction()
    {
        return Category::with(["products"])->get();
    }
} 