<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 05.02.14
 * Time: 14:30
 */

class IndexController extends BaseController
{
    public function indexAction()
    {
        return View::make("index");
    }
} 