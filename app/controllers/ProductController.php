<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 05.02.14
 * Time: 12:34
 */

class ProductController extends BaseController
{
    public function indexAction()
    {
        $query = Product::with("category");
        $category = Input::get("category");

        if ($category) {
            $query->where("category_id", $category);
        }

        return $query->get();
    }
} 