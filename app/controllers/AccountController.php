<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 05.02.14
 * Time: 12:46
 */

class AccountController extends BaseController
{
    public function authenticateAction()
    {
        $credentials = [
            "email" => Input::get("email"),
            "password" => Input::get("password")
        ];

        if (Auth::attempt($credentials)) {
            return Response::json([
                "status" => "ok",
                "account" => Auth::user()->toArray()
            ]);
        }

        return Response::json([
            "status" => "error"
        ]);
    }
} 