<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 27.01.14
 * Time: 16:30
 */

//use Formariv\Chat\Command;

require dirname(__DIR__) . '/workbench/formativ/chat/src/Formariv/Chat/Command/Serve.php';

require dirname(__DIR__) . '/vendor/autoload.php';

$server = new Serve();

$server->fire();