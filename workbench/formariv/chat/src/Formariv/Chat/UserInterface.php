<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 20.01.14
 * Time: 12:37
 */

namespace Formariv\Chat;

use Evenement\EventEmitterInterface;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

interface UserInterface {
    public function getSocket();
    public function setSocket(ConnectionInterface $socket);
    public function getId();
    public function setId($id);
    public function getName();
    public function setName($name);
} 