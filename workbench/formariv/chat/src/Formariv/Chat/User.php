<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 20.01.14
 * Time: 12:39
 */

namespace Formariv\Chat;

use Ratchet\ConnectionInterface;

class User implements UserInterface
{
    protected $socket,
        $id,
        $name;

    public function getSocket()
    {
        return $this->socket;
    }

    public function setSocket(ConnectionInterface $socket)
    {
        $this->socket = $socket;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
} 