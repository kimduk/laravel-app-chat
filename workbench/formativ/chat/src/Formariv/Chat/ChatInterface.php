<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 17.01.14
 * Time: 13:40
 */

namespace Formariv\Chat;

use Evenement\EventEmitterInterface;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

interface ChatInterface extends MessageComponentInterface
{
    public function getUserBySocket(ConnectionInterface $socket);
    public function getEmitter();
    public function setEmitter(EventEmitterInterface $emitter);
    public function getUsers();

}